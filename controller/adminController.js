const nerdamer = require('nerdamer');

exports.calculateEquations = (req, res, next) => {

    const equation = req.body.equation;
    const params = req.body.params;

    let e = nerdamer(equation,params);
    let result = e.text();
    
    if (result){
        res.status(201).json({
            status: 1,
            message: 'The equation is calculated successfully',
            result: result
        });
    }else{
        res.status(200).json({
            status: 0,
            message: 'Invalid result',
            result: result
        }); 
    }
}